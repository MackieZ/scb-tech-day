import { BuypagePageModule } from './../pages/buypage/buypage.module';
import { DashboardPageModule } from './../pages/dashboard/dashboard.module';
import { PromotionPageModule } from './../pages/promotion/promotion.module';
import { CurrencyRate_2PageModule } from './../pages/currency-rate-2/currency-rate-2.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CurrencyRate_2PageModule,
    PromotionPageModule,
    DashboardPageModule,
    BuypagePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
