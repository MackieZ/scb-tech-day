import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CurrencyRate_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-currency-rate-2',
  templateUrl: 'currency-rate-2.html',
})
export class CurrencyRate_2Page {

  currentStep: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currentStep = 'BUY';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurrencyRate_2Page');
  }

  radioCheck(inp: string) {
    this.currentStep = inp;
  }

}
