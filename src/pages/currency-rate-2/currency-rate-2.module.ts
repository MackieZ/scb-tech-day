import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrencyRate_2Page } from './currency-rate-2';

@NgModule({
  declarations: [
    CurrencyRate_2Page,
  ],
  imports: [
    IonicPageModule.forChild(CurrencyRate_2Page),
  ],
})
export class CurrencyRate_2PageModule {}
