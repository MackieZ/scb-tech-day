import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExratePage } from './exrate';

@NgModule({
  declarations: [
    ExratePage,
  ],
  imports: [
    IonicPageModule.forChild(ExratePage),
  ],
})
export class ExratePageModule {}
