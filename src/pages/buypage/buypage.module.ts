import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuypagePage } from './buypage';

@NgModule({
  declarations: [
    BuypagePage,
  ],
  imports: [
    IonicPageModule.forChild(BuypagePage),
  ],
})
export class BuypagePageModule {}
