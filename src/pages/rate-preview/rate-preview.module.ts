import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RatePreviewPage } from './rate-preview';

@NgModule({
  declarations: [
    RatePreviewPage,
  ],
  imports: [
    IonicPageModule.forChild(RatePreviewPage),
  ],
})
export class RatePreviewPageModule {}
